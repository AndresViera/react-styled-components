import styled, {createGlobalStyle} from 'styled-components';
import Formulario from './components/Formulario';
import ListaCitas from './components/ListaCitas';
import Estilos from './components/Estilos';
const GlobalStyle = createGlobalStyle`
  html{
    height: 100%;
    background: transparent;
  }

  *,*::after, ::before{
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }

  body{
    font-family: Arial, Helvetivca, sans-serif;
    height: 100%;
    margin: 0;
    box-sizing: border-box;
    color: #555;
  }

`;

const MiButton = styled.button`
/* Adapt the colors based on primary prop */
background: ${props => props.primary ? "palevioletred" : "white"};
color: ${props => props.primary ? "white" : "palevioletred"};
font-size: 1em;
margin: 1em;
padding: 0.25em 1em;
border: 2px solid palevioletred;
border-radius: 3px;
`;

const Container = styled.div`
  max-width: 1140px;
  background-color: #f6f6f6;
  margin: 0 auto;
`;

function App() {
  return (
    <div className="App">
      <GlobalStyle/>
      <Container/>
      <Formulario/>
      <ListaCitas/>
      
    </div>
  );
}

export default App;
