import React, { Component } from 'react';
import * as Estilos from '../Estilos';
import Title from '../Title';

export default class Formulario extends Component {
  render() {
    return (
        <div>
            <Title titulo={'FORMULARIO DE CITAS'}/>
                <Estilos.Form>        
                <Estilos.FormGroup col='2'>
                    <label htmlFor="">Nombre</label>
                    <Estilos.Input type="text" name="nombre" placeholder='Ingrese el nombre' />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='2'>
                    <label htmlFor="">Apellido</label>
                    <Estilos.Input type="text" name="apelido" placeholder='Ingrese el apellido' />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='1'>
                    <label htmlFor="">Direccion</label>
                    <Estilos.Input type="text" name="direccion" placeholder='Ingrese el dirección' />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='3'>
                    <label htmlFor="">Fecha de cita</label>
                    <Estilos.Input type="date" name="fecha" />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='3'>
                    <label htmlFor="">Hora de cita</label>
                    <Estilos.Input type="time" name="hora" />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='3'>
                    <label htmlFor="">Sintomas</label>
                    <Estilos.TextArea type="text" name="sintomas" />
                </Estilos.FormGroup>

                <Estilos.FormGroup col='1'>
                    <Estilos.Button type='submit'>Reservar</Estilos.Button>
                </Estilos.FormGroup>
                
            </Estilos.Form>
        </div>
    )
 }
}
