import React from 'react';
import styled from 'styled-components';

const TitleForm = styled.h2`
    color: '#7271fb';
    text-align: center;
    margin: 0;
`;

const Title = ({titulo}) => {
    return (
        <TitleForm>{titulo}</TitleForm>
    );
};

export default Title;